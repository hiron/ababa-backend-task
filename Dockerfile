FROM node:18-alpine

WORKDIR /ababa_task_backend

COPY package.json /ababa_task_backend/package.json

RUN npm install

ADD . .

EXPOSE 8000

CMD ["npm", "run", "start:dev"]


