import { IsEmail, IsNotEmpty, Length } from "class-validator";

export class LoginUserDto{
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;
}