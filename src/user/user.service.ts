import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './models/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  async createUser(user: CreateUserDto): Promise<any>{
    user.password = await bcrypt.hash(user.password, 10);
    const {password, ...result} = await this.usersRepository.save(user);
    return result;
  }

  findOne(email: string): Promise<User| undefined>{
    return this.usersRepository.findOneBy({email});
  }
}
