import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RateDto } from './dto/rate.dto';
import { Rate } from './models/rate.entity';

@Injectable()
export class RateService {
  constructor(
    @InjectRepository(Rate)
    private rateRepository: Repository<Rate>,
  ) {}

  private addRate(userId: number, rateDto: RateDto, filmId: number) {
    return this.rateRepository.save({
      ...rateDto,
      user: { id: userId },
      film: { id: filmId },
    });
  }

  private updateRate(userId: number, rateDto: RateDto, filmId: number) {
    return this.rateRepository.update(
      { user: { id: userId }, film: { id: filmId } },
      { ...rateDto },
    );
  }

  async addOrUpdateRating(userId: number, rateDto: RateDto, filmId: number) {
    let rating = await this.rateRepository.findOneBy({
      user: { id: userId },
      film: { id: filmId },
    });

    if (!!rating) {
      return this.updateRate(userId, rateDto, filmId);
    }

    return this.addRate(userId, rateDto, filmId);
  }
}
