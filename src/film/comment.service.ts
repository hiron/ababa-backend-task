import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';
import { CommentDto } from './dto/comment.dto';
// import { UpdateCommentDto } from './dto/update_comment.dto';
import { Comment } from './models/comment.entity';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private commentRepository: Repository<Comment>,
  ) {}

  addComment(userId: number, comment: CommentDto, filmId: number) {
    return this.commentRepository.save({
      message: comment.message,
      user: { id: userId },
      film: { id: filmId },
    });
  }

  updateComment(id: number, userId: number, comment: CommentDto) {
    return this.commentRepository.update(
      { id, user: { id: userId } },
      { ...comment },
    );
    //   .createQueryBuilder()
    //   .update({ message: comment.message })
    //   .where({ id: comment.id })
    //   .returning('*')
    //   .execute()
    //   .then((result) => result.raw[0]);
  }

  deleteComment(id: number, userId: number) {
    return this.commentRepository.delete({ id, user: { id: userId } });
    //   .createQueryBuilder('comments')
    //   .where({ id })
    //   .andWhere('comments.userId = :userId', {userId})
    //   .delete()
    //   .execute();
  }
}
