import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Request,
  Response,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CommentService } from './comment.service';
import { CommentDto } from './dto/comment.dto';
import { RateDto } from './dto/rate.dto';
import { FilmService } from './film.service';
import { RateService } from './rate.service';

@Controller('film')
export class FilmController {
  constructor(
    private filmService: FilmService,
    private commentService: CommentService,
    private rateService: RateService,
  ) {}

  @Get()
  getFilms() {
    return this.filmService.getAllFilms();
  }

  @Get(':id')
  getOneFilm(@Param('id') id: number) {
    return this.filmService.getOneFilm(id);
  }

  //   @Get('saveall')
  //   async saveAll(){
  //     return await this.filmService.insertAllData();
  //   }

  @UseGuards(JwtAuthGuard)
  @Post(':id/comment')
  addComment(
    @Request() req,
    @Param('id') filmId,
    @Body() comment: CommentDto,
  ): Promise<any> {
    return this.commentService.addComment(req.user.userId, comment, filmId);
  }

  @UseGuards(JwtAuthGuard)
  @Post('comment/:id')
  updateComment(
    @Request() req,
    @Param('id') commentId,
    @Body() comment: CommentDto,
  ): Promise<any> {
    return this.commentService.updateComment(
      commentId,
      req.user.userId,
      comment,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Delete('comment/:id')
  deleteComment(@Request() req, @Param('id') commentId): Promise<any> {
    return this.commentService.deleteComment(commentId, req.user.userId);
  }

  @UseGuards(JwtAuthGuard)
  @Post(':id/rate')
  addRating(@Request() req, @Param('id') filmId, @Body() rateDto: RateDto) {
    return this.rateService.addOrUpdateRating(req.user.userId, rateDto, filmId);
  }
}
