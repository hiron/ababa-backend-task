import { User } from 'src/user/models/user.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { Film } from './film.entity';

@Entity('rates')
@Unique(['film', 'user'])
export class Rate {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'numeric', precision: 1, scale: 0 })
  rate: number;

  @ManyToOne(() => Film, (film) => film.rates)
  film: Film;

  @ManyToOne(() => User)
  user: User;
}
