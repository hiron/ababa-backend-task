import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Comment } from './comment.entity';
import { Rate } from './rate.entity';

@Entity('films')
export class Film {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;
  @Column({ nullable: true })
  image: string;
  @Column({ name: 'movie_banner', nullable: true })
  movieBanner: string;
  @Column()
  description: string;
  @Column()
  director: string;
  @Column()
  producer: string;
  @Column()
  release: string;
  @Column({ name: 'running_time' })
  runningTime: string;

  @OneToMany(() => Rate, (rate) => rate.film)
  rates: Rate[];

  @OneToMany(() => Comment, (comment) => comment.film)
  comments: Comment[];
}
