import { Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Film } from './models/film.entity';
import { FILMS } from './models/films_json_data';

@Injectable()
export class FilmService {
  constructor(
    @InjectRepository(Film)
    private readonly filmRepository: Repository<Film>,
  ) {}

  getAllFilms(): Promise<Film[]> {
    return this.filmRepository.find();
  }

  getOneFilm(id: number): Promise<any> {
    return this.filmRepository
      .createQueryBuilder('f')
      .where('f.id = :id', { id })
      .leftJoinAndSelect('f.rates', 'r')
      .leftJoinAndSelect('f.comments', 'c')
      .leftJoinAndSelect('c.user', 'u')
      .getOne();
  }

  //   async insertAllData() {
  //     return await Promise.all(
  //       FILMS.map(
  //         ({
  //           title,
  //           image,
  //           movie_banner,
  //           description,
  //           director,
  //           producer,
  //           release_date,
  //           running_time,
  //         }) =>
  //           this.filmRepository.save({
  //             title,
  //             image,
  //             movieBanner: movie_banner,
  //             description,
  //             director,
  //             producer,
  //             release: release_date,
  //             runningTime: running_time,
  //           }),
  //       ),
  //     );
  //   }
}
