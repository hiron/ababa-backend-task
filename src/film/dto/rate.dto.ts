import { IsNotEmpty } from 'class-validator';

export class RateDto {
  @IsNotEmpty()
  rate: number;
}
