import { IsNotEmpty } from "class-validator";
import { CommentDto } from "./comment.dto";

export class UpdateCommentDto extends CommentDto{
    @IsNotEmpty()
    id: number
}