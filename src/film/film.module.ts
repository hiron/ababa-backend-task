import { Module } from '@nestjs/common';
import { FilmController } from './film.controller';
import { FilmService } from './film.service';
import { CommentService } from './comment.service';
import { RateService } from './rate.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Film } from './models/film.entity';
import { Comment } from './models/comment.entity';
import { Rate } from './models/rate.entity';
import { AuthModule } from 'src/auth/auth.module';
import { User } from 'src/user/models/user.entity';
import { UserModule } from 'src/user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([Film, Comment, Rate]), AuthModule],
  controllers: [FilmController],
  providers: [FilmService, CommentService, RateService],
})
export class FilmModule {}
